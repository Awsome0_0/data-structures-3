#include <iostream>
#include <conio.h>
#include <list>
using namespace std;

class Node {
public:
		string	data;
		string	description;
		Node*	next;
};
class Names{
public:
		string names;
		list  <Node> CustMovies;
		Node* next;	
};
class List{
	public:
			List(void)	{head = NULL;	}
			~List(void);
			
			bool isEmpty(){ return head == NULL;	}
			Node* InsertNode(int index,string x,string y);
			int FindNode(string x);
			int DeleteNode(string x);
			void DisplayList(void);
			string FindDesc(int index);
	private:
			Node* head;
};
string List::FindDesc(int index){
	Node* CurrNode	=	head;
	int	CurrIndex	=	1;
	while (CurrIndex != index){
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	if (CurrNode) return CurrNode->description;
	return NULL;
}
int List::FindNode(string x){
	Node* CurrNode	=	head;
	int	CurrIndex	=	1;
	while (CurrNode && CurrNode->data != x){
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	if (CurrNode) return CurrIndex;
	return 0;
}
int List:: DeleteNode(string x){
	Node* prevNode	=	NULL;
	Node* CurrNode	=	head;
	int CurrIndex	=	1;
	while (CurrNode && CurrNode->data != x){
		prevNode	=	CurrNode;
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	if (CurrNode){
		if(prevNode){
			prevNode->next	=	CurrNode->next;
			delete CurrNode;
		}
		else{
			head	=	CurrNode->next;
			delete	CurrNode;
		}
		return	CurrIndex;
	}
	return 0;
}
void List::DisplayList(){
	int num = 0;
	Node* CurrNode	=	head;
	while (CurrNode != NULL){
		cout<<"["<<num+1<<"]"<<CurrNode->data<<endl;
		CurrNode	=	CurrNode->next;
		num++;
	}
	
}
Node* List::InsertNode(int index,string x,string y){
	if (index < 0) return NULL;
	int CurrIndex = 1;
	Node* CurrNode =head;
	while (CurrNode && index > CurrIndex){
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	Node* newNode =		new		Node;
	newNode->data = 	x;
	newNode->description = y;
	if (index==0){
			newNode->next	=	head;
			head 			=	newNode;
	}
	else{
		newNode->next	=	CurrNode->next;
		CurrNode->next	=	newNode;
	}
	return newNode;
}
List::~List(void){
	Node* CurrNode	=	head,	*nextNode	=	NULL;
	while (CurrNode != NULL){
		nextNode	=	CurrNode->next;
		delete	CurrNode;
		CurrNode	=	nextNode;
	}
}
int main(){
	int mov, menu;
	List MovieList;
	MovieList.InsertNode(0,"Star Wars: Episode VII - The Force Awakens (2015)"," J.J. Abrams");
	MovieList.InsertNode(1,"Avengers: Endgame (2019)","Anthony Russo, Joe Russo");
	MovieList.InsertNode(2,"Avatar (2009)","James Cameron");
	MovieList.InsertNode(3,"Black Panther (2018)","Ryan Coogler");
	MovieList.InsertNode(4,"Avengers: Infinity War (2018)","Anthony Russo");
	MovieList.InsertNode(5,"Titanic (1997)","James Cameron");
	MovieList.InsertNode(6,"Jurassic World (2015)","Colin Trevorrow");
	MovieList.InsertNode(7,"Marvel's The Avengers (2012)","Joss Whedon");
	MovieList.InsertNode(8,"Star Wars: Episode VIII - The Last Jedi (2017)","Rian Johnson");
	MovieList.InsertNode(9,"Incredibles 2 (2018)","Brad Bird");
	MovieList.InsertNode(10,"The Dark Knight (2008)","Christopher Nolan");
	MovieList.InsertNode(11,"Rogue One: A Star Wars Story (2016)","Gareth Edwards");
	MovieList.InsertNode(12,"Beauty and the Beast (2017)","Bill Condon");
	MovieList.InsertNode(13,"Finding Dory (2016)","Andrew Stanton");
	MovieList.InsertNode(14,"Star Wars: Episode I - The Phantom Menace (1999)","George Lucas");
	MovieList.InsertNode(15,"Star Wars: Episode IV - A New Hope (1977)","George Lucas");
	MovieList.InsertNode(16,"Avengers: Age of Ultron (2015)","Joss Whedon");
	MovieList.InsertNode(17,"The Dark Knight Rises (2012)","Christopher Nolan");
	MovieList.InsertNode(18,"Shrek 2 (2004)","Andrew Adamson, Conrad Vernon, Kelly Asbury");
	MovieList.InsertNode(19,"E. T. The Extra-Terrestrial (1982)","Stephen Spielberg");
	MovieList.InsertNode(20,"Captain Marvel (2019)","Anna Boden, Ryan Fleck");
	MovieList.InsertNode(21,"The Hunger Games: Catching Fire (2013)","Francis Lawrence");
	MovieList.InsertNode(22,"Pirates of the Caribbean: Dead Man's Chest (2006)","Gore Vebinski");
	MovieList.InsertNode(23,"The Lion King (1994)","Jon Favreau");
	MovieList.InsertNode(24,"Jurassic World: Fallen Kingdom (2018)","J.A. Bayona");
	cout<<"Welcome to the shop\nType 1 to get the list of movies\nType 2 to see the description of the movies"<<endl;
	cin>>menu;
	if (menu == 1){
		MovieList.DisplayList();
	}
	else if(menu == 2){
		MovieList.DisplayList();
		cout<<"select the number of the movie you want to know more of: "<<endl;
		cin>>mov;
		cout<<MovieList.FindDesc(mov)<<endl;
	}
}